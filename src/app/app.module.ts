import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProjectSmallComponent } from './components/project-small/project-small.component';
import { ProjectFullscreenComponent } from './components/project-fullscreen/project-fullscreen.component';
import { S5buttonComponent } from './scalpel/s5button/s5button.component';
import { BasicAgencyComponent } from './scalpel/basic-agency/basic-agency.component';
import { SuperrbComponent } from './scalpel/superrb/superrb.component';
import { UltranoirComponent } from './scalpel/ultranoir/ultranoir.component';

@NgModule({
  declarations: [
    AppComponent,
    ProjectSmallComponent,
    ProjectFullscreenComponent,
    S5buttonComponent,
    BasicAgencyComponent,
    SuperrbComponent,
    UltranoirComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
